ansible-role-ntp-server
=========

Install and configure chrony as NTP client and server.

Requirements
------------

none

Role Variables
--------------

none

Dependencies
------------

none

Example Playbook
----------------

```
    - hosts: servers
      roles:
         - ansible-role-ntp-server
```

License
-------

The Unlicense

Author Information
------------------

Written by and for Freifunk Chemnitz.
